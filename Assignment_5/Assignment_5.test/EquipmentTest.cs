﻿using System;
using Xunit;
using Assignment_5.Custom_Exception;

namespace Assignment_5.test
{
    public class EquipmentTest
    {
        [Fact]
        public void EquipTooHighWeapon_CheckWeaponLevel_InvalidWeaponException()
        {
            Warrior npc = new() { Name = "warrior" };
            Weapon meatCleaver = new()
            {
                Name = "Meat cleaver",
                Level = 2,
                slot = Equipment.Slot.Weapons,
                weaponTypes = Weapon.WeaponTypes.Axes,
                equipmentAttributes = new(3, 0, 0, 1),
                Dmg = 5,
                AttackSpeed = 6
            };

            Assert.Throws<InvalidWeaponException>(() => npc.EquipmentManager.CheckWeaponLevel(meatCleaver, npc));
        }

        [Fact]
        public void EquipTooHighArmour_CheckArmourLevel_InvalidArmourException()
        {
            Warrior npc = new() { Name = "warrior" };
            Armour darkKnightArmour = new()
            {
                Name = "Dark Knight armour",
                Level = 100,
                slot = Equipment.Slot.Body,
                armourTypes = Armour.ArmourTypes.Mail,
                equipmentAttributes = new(999, 999, 999, 999)
            };

            Assert.Throws<InvalidArmourException>(() => npc.EquipmentManager.CheckArmourLevel(darkKnightArmour, npc));
        }

        [Fact]
        public void EquipWrongTypeOfWeapon_ChecckWeaponType_InvalidWeaponException()
        {
            Warrior npc = new() { Name = "Goliath" };
            Weapon batRang = new()
            {
                Name = "Bat Rang",
                Level = 1,
                slot = Equipment.Slot.Weapons,
                weaponTypes = Weapon.WeaponTypes.Daggers,
                equipmentAttributes = new(5, 30, 0, 0),
                Dmg = 100,
                AttackSpeed = 1000
            };

            Assert.Throws<InvalidWeaponException>(() => npc.EquipmentManager.CheckWeaponType(batRang, npc));
        }

        [Fact]
        public void EquipWrongTypeOfArmour_ChecckArmourType_InvalidWeaponException()
        {
            Warrior npc = new() { Name = "Goliath" };
            Armour blueTurban = new()
            {
                Name = "Blue Turban",
                Level = 1,
                slot = Equipment.Slot.Head,
                armourTypes = Armour.ArmourTypes.Cloth,
                equipmentAttributes = new(0, 1, 3, 1)
            };

            Assert.Throws<InvalidArmourException>(() => npc.EquipmentManager.CheckArmourType(blueTurban, npc));
        }

        [Fact]
        public void EquipWeapon_EquipWeapon_SendMessage()
        {
            Warrior npc = new() { Name = "Stukov" };
            Weapon meatCleaver = new()
            {
                Name = "Meat cleaver",
                Level = 1,
                slot = Equipment.Slot.Weapons,
                weaponTypes = Weapon.WeaponTypes.Axes,
                equipmentAttributes = new(3, 0, 0, 1),
                Dmg = 5,
                AttackSpeed = 6
            };
            string expected = "New Weapon Equipped!";

            string actual = npc.EquipmentManager.EquipWeapon(meatCleaver);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmour_EquipArmour_SendMessage()
        {
            Warrior npc = new() { Name = "Stukov" };
            Armour knightArmour = new()
            {
                Name = "Knight armour",
                Level = 1,
                slot = Equipment.Slot.Body,
                armourTypes = Armour.ArmourTypes.Plate,
                equipmentAttributes = new(4, 1, 0, 7)
            };
            string expected = "New Armour Equipped!";

            string actual = npc.EquipmentManager.EquipArmour(knightArmour);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDpsWithNoWeapon_CalcDps_IsEqual()
        {
            Warrior npc = new() { Name = "Durin" };
            float expected = MathF.Floor(1 + 5 * 0.01f);

            float actual = npc.DPS;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDpsWithValidWeapon_CalcDps_IsEqual()
        {
            Warrior npc = new() { Name = "Durin" };
            Weapon meatCleaver = new()
            {
                Name = "Meat cleaver",
                Level = 1,
                slot = Equipment.Slot.Weapons,
                weaponTypes = Weapon.WeaponTypes.Axes,
                equipmentAttributes = new(3, 0, 0, 1),
                Dmg = 5,
                AttackSpeed = 6
            };
            npc.EquipmentManager.EquipWeapon(meatCleaver);
            float expected = MathF.Floor((5 * 6) * (1 + 8 * 0.01f));

            float actual = npc.DPS;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDpsWithValidWeaponAndValidArmour_CalcDps_IsEqual()
        {
            Warrior npc = new() { Name = "Durin" };
            Weapon meatCleaver = new()
            {
                Name = "Meat cleaver",
                Level = 1,
                slot = Equipment.Slot.Weapons,
                weaponTypes = Weapon.WeaponTypes.Axes,
                equipmentAttributes = new(3, 0, 0, 1),
                Dmg = 5,
                AttackSpeed = 6
            };
            Armour knightArmour = new()
            {
                Name = "Knight armour",
                Level = 1,
                slot = Equipment.Slot.Body,
                armourTypes = Armour.ArmourTypes.Plate,
                equipmentAttributes = new(4, 1, 0, 7)
            };
            npc.EquipmentManager.EquipWeapon(meatCleaver);
            npc.EquipmentManager.EquipArmour(knightArmour);
            float expected = MathF.Floor((5 * 6) * (1 + 12 * 0.01f));

            float actual = npc.DPS;

            Assert.Equal(expected, actual);
        }
    }
}
