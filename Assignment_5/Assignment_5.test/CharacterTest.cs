using System;
using Xunit;

namespace Assignment_5.test
{
    public class CharacterTest
    {
        [Fact]
        public void CharacterInitialLevel_AtSetup_EqualsOne()
        {
            Ranger npc = new() { Name = "Legolas"};
            int expected = 1;

            int actual = npc.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterLevelUp_LevelUp_IncreaseOneLevel()
        {
            Mage npc = new() { Name = "Harry" };
            npc.LevelUp(1);
            int expected = 2;

            int actual = npc.Level;

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void CharacterLevelUpWithBadInput_LevelUp_ArgumentException(int badLevel)
        {
            Mage npc = new() { Name = "Gustavo" };
            Assert.Throws<ArgumentException>(() => npc.CheckLevel(badLevel));
        }

        [Fact]
        public void MageAttributesAreSetupCorrectly_SetPrimaryAttributes_CorrectlySetUp()
        {
            Mage npc = new() { Name = "Alexei" };
            PrimaryAttributes expected = new(1, 1, 8, 5);

            PrimaryAttributes actual = npc.PrimaryAttributes;

            Assert.Equal(expected, actual);
        }
        
        [Fact]
        public void RangerAttributesAreSetupCorrectly_SetPrimaryAttributes_CorrectlySetUp()
        {
            Ranger npc = new() { Name = "Alexei" };
            PrimaryAttributes expected = new(1, 7, 1, 8);

            PrimaryAttributes actual = npc.PrimaryAttributes;

            Assert.Equal(expected, actual);
        }
        
        [Fact]
        public void RogueAttributesAreSetupCorrectly_SetPrimaryAttributes_CorrectlySetUp()
        {
            Rogue npc = new() { Name = "Alexei" };
            PrimaryAttributes expected = new(2, 6, 1, 8);

            PrimaryAttributes actual = npc.PrimaryAttributes;

            Assert.Equal(expected, actual);
        }
        
        [Fact]
        public void WarriorAttributesAreSetupCorrectly_SetPrimaryAttributes_CorrectlySetUp()
        {
            Warrior npc = new() { Name = "Alexei" };
            PrimaryAttributes expected = new(5, 2, 1, 10);

            PrimaryAttributes actual = npc.PrimaryAttributes;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MageIncreaseAttributesWhenLevelUp_LevelUp_CorrectlySetUp()
        {
            Mage npc = new() { Name = "Alexei" };
            npc.LevelUp(1);
            PrimaryAttributes expected = new(2, 2, 13, 8);

            PrimaryAttributes actual = npc.PrimaryAttributes;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RangerIncreaseAttributesWhenLevelUp_LevelUp_CorrectlySetUp()
        {
            Ranger npc = new() { Name = "Alexei" };
            npc.LevelUp(1);
            PrimaryAttributes expected = new(2, 12, 2, 10);

            PrimaryAttributes actual = npc.PrimaryAttributes;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RogueIncreaseAttributesWhenLevelUp_LevelUp_CorrectlySetUp()
        {
            Rogue npc = new() { Name = "Alexei" };
            npc.LevelUp(1);
            PrimaryAttributes expected = new(3, 10, 2, 11);

            PrimaryAttributes actual = npc.PrimaryAttributes;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WarriorIncreaseAttributesWhenLevelUp_LevelUp_CorrectlySetUp()
        {
            Warrior npc = new() { Name = "Alexei" };
            npc.LevelUp(1);
            PrimaryAttributes expected = new(8, 4, 2, 15);

            PrimaryAttributes actual = npc.PrimaryAttributes;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WarriorIncreaseSecondaryAttributesWhenLevelUp_LevelUp_CorrectlySetUp()
        {
            Warrior npc = new() { Name = "Alexei" };
            npc.LevelUp(1);
            SecondaryAttributes expected = new(150, 12, 2);

            SecondaryAttributes actual = npc.SecondaryAttributes;

            Assert.Equal(expected, actual);
        }
    }
}
