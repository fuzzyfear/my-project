﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment_5.Custom_Exception;

namespace Assignment_5
{
    public class EquipmentManager : Requirements
    {
        public Character Character { get; private set; }
        public EquipmentManager(Character character)
        {
            Character = character;
        }

        /// <summary>
        /// Equips a weapon.
        /// </summary>
        /// <param name="weapon"></param>
        public string EquipWeapon(Weapon weapon)
        {
            try
            {
                CheckWeaponType(weapon, Character);
                CheckWeaponLevel(weapon, Character);
                CheckWeaponSlot(weapon, Character);
                Character.characterSlots.Add(weapon.slot, weapon);
                Character.SetBonusPrimaryAttributes(weapon.equipmentAttributes);
                return "New Weapon Equipped!";
            }
            catch (InvalidWeaponException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return "";
        }

        /// <summary>
        /// Equips an armour.
        /// </summary>
        /// <param name="armour"></param>
        public string EquipArmour(Armour armour)
        {
            try
            {
                CheckArmourType(armour, Character);
                CheckArmourLevel(armour, Character);
                CheckArmourSlot(armour, Character);
                Character.characterSlots.Add(armour.slot, armour);
                Character.SetBonusPrimaryAttributes(armour.equipmentAttributes);
                return "New Armour Equipped!";
            }
            catch (InvalidArmourException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return "";
        }
    }
}
