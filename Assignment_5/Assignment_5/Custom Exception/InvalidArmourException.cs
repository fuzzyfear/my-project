﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_5.Custom_Exception
{
    public class InvalidArmourException : Exception
    {
        public InvalidArmourException(string message) : base(message){}
    }
}
