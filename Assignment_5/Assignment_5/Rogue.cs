﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_5
{
    public class Rogue : Character, IStalk
    {
        public override List<Armour.ArmourTypes> ArmourCompatibility { get; set; } = new()
        {
            Armour.ArmourTypes.Leather,
            Armour.ArmourTypes.Mail
        };
        public override List<Weapon.WeaponTypes> WeaponCompatibility { get; set; } = new()
        {
            Weapon.WeaponTypes.Daggers,
            Weapon.WeaponTypes.Swords
        };

        public override void CalcDps()
        {
            Equipment equiptedWeapon;
            characterSlots.TryGetValue(Equipment.Slot.Weapons, out equiptedWeapon);
            if (equiptedWeapon != null)
            {
                Weapon weapons = (Weapon)equiptedWeapon;
                DPS = MathF.Floor((weapons.Dmg * weapons.AttackSpeed) * (1 + TotalPrimaryAttributes.Dexterity * 0.01f));
            }
            else
                DPS = MathF.Floor(1 + TotalPrimaryAttributes.Dexterity * 0.01f);
        }

        public override void LevelUp(int level)
        {
            try
            {
                CheckLevel(level);
                for (int i = 0; i < level; i++)
                    PrimaryAttributes += new PrimaryAttributes(1, 4, 1, 3);
                Level += level;
                SetTotalPrimaryAttributes();
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public override void SetPrimaryAttributes() => PrimaryAttributes = new PrimaryAttributes(2, 6, 1, 8);

        public void StalkPrey(Character prey)
        {
            if(this.PrimaryAttributes.Dexterity * 2 > prey.PrimaryAttributes.Intelligence)
                Console.WriteLine("Follow the target through the crowd without being detected");
            else Console.WriteLine("You lost the target");
        }
    }
}
