﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_5
{
    public interface IStalk
    {
        /// <summary>
        /// Stalks the prey
        /// </summary>
        /// <param name="character"></param>
        public void StalkPrey(Character character);
    }
}
