﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_5
{
    public abstract class Character
    {
        public Dictionary<Equipment.Slot, Equipment> characterSlots = new();

        public PrimaryAttributes PrimaryAttributes { get; set; }
        public PrimaryAttributes BonusPrimaryAttributes { get; set; }
        public PrimaryAttributes TotalPrimaryAttributes { get; set; }
        public SecondaryAttributes SecondaryAttributes { get; set; }
        public EquipmentManager EquipmentManager { get; private set; }
        public string Name { get; set; }
        public int Level { get; protected set; } = 1;
        public float DPS { get; protected set; }

        public Character()
        {
            EquipmentManager = new(this);
            SetPrimaryAttributes();
            TotalPrimaryAttributes = PrimaryAttributes;
            SetSecondaryAttributes();
        }

        public abstract List<Armour.ArmourTypes> ArmourCompatibility { get; set; }
        public abstract List<Weapon.WeaponTypes> WeaponCompatibility { get; set; }

        /// <summary>
        /// Calculates the damage per seconds (dps) of the character.
        /// </summary>
        public abstract void CalcDps();

        /// <summary>
        /// Sets the values of the character's primary attributes.
        /// </summary>
        public abstract void SetPrimaryAttributes();

        /// <summary>
        /// Increases the level of the character according to the argument and increases the value of all primary attributes.
        /// </summary>
        public abstract void LevelUp(int level);

        /// <summary>
        /// Checks if the amount of levels gained is at least 1
        /// </summary>
        public void CheckLevel(int level)
        {
            if (level <= 0)
                throw new ArgumentException("You need to gain at least 1 level");
        }

        /// <summary>
        /// Adds the attributes of an equipment to the character.
        /// </summary>
        /// <param name="equipmentAttributes"></param>
        public void SetBonusPrimaryAttributes(PrimaryAttributes equipmentAttributes)
        {
            BonusPrimaryAttributes += equipmentAttributes;
            SetTotalPrimaryAttributes();
        }

        /// <summary>
        /// Calculates every attributes of the character.
        /// </summary>
        public void SetTotalPrimaryAttributes()
        {
            TotalPrimaryAttributes = PrimaryAttributes + BonusPrimaryAttributes;
            SetSecondaryAttributes();
        }

        /// <summary>
        /// Calculates the secondary attributes based on the total primary attributes.
        /// </summary>
        public void SetSecondaryAttributes()
        {
            SecondaryAttributes = new SecondaryAttributes(
                TotalPrimaryAttributes.Vitality * 10,
                TotalPrimaryAttributes.Strength + TotalPrimaryAttributes.Dexterity, 
                TotalPrimaryAttributes.Intelligence
                );
            CalcDps();
        }

        /// <summary>
        /// DIsplays the character's attributes.
        /// </summary>
        public void DisplayStats()
        {
            StringBuilder stringBuilder = new();
            stringBuilder.Append($"Character name: {Name}\n");
            stringBuilder.Append($"Character level: {Level}\n");
            stringBuilder.Append($"Strength: {TotalPrimaryAttributes.Strength}\n");
            stringBuilder.Append($"Dexterity: {TotalPrimaryAttributes.Dexterity}\n");
            stringBuilder.Append($"Intelligence: {TotalPrimaryAttributes.Intelligence}\n");
            stringBuilder.Append($"Health: {SecondaryAttributes.Health}\n");
            stringBuilder.Append($"Armour rating: {SecondaryAttributes.ArmourRating}\n");
            stringBuilder.Append($"Elemental resisztance: {SecondaryAttributes.ElementalResistance}\n");
            stringBuilder.Append($"DPS: {DPS}\n");
            Console.WriteLine(stringBuilder);
        }
    }
}
