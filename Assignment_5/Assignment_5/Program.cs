﻿using System;
using Assignment_5.Custom_Exception;

namespace Assignment_5
{
    public class Program
    {
        static void Main(string[] args)
        {
            Weapon woddenStaff = new()
            {
                Name = "Wodden Staff",
                Level = 1,
                slot = Equipment.Slot.Weapons,
                weaponTypes = Weapon.WeaponTypes.Staffs,
                equipmentAttributes = new(0, 0, 2, 0),
                Dmg = 4,
                AttackSpeed = 2
            };
            Weapon meatCleaver = new()
            {
                Name = "Meat cleaver",
                Level = 1,
                slot = Equipment.Slot.Weapons,
                weaponTypes = Weapon.WeaponTypes.Axes,
                equipmentAttributes = new(3, 0, 0, 1),
                Dmg = 5,
                AttackSpeed = 6
            };
            Weapon batRang = new()
            {
                Name = "Bat Rang",
                Level = 24,
                slot = Equipment.Slot.Weapons,
                weaponTypes = Weapon.WeaponTypes.Daggers,
                equipmentAttributes = new(5, 30, 0, 0),
                Dmg = 100,
                AttackSpeed = 1000
            };
            Armour blueTurban = new()
            {
                Name = "Blue Turban",
                Level = 1,
                slot = Equipment.Slot.Head,
                armourTypes = Armour.ArmourTypes.Cloth,
                equipmentAttributes = new(0, 1, 3, 1)
            };
            Armour knightArmour = new()
            {
                Name = "Knight armour",
                Level = 1,
                slot = Equipment.Slot.Body,
                armourTypes = Armour.ArmourTypes.Plate,
                equipmentAttributes = new(4, 1, 0, 7)
            };
            Armour darkKnightArmour = new()
            {
                Name = "Dark Knight armour",
                Level = 100,
                slot = Equipment.Slot.Body,
                armourTypes = Armour.ArmourTypes.Mail,
                equipmentAttributes = new(999, 999, 999, 999)
            };
            Warrior npc = new() { Name = "Durin" };
            Rogue player = new() { Name = "Poe" };
            player.StalkPrey(npc);
        }
    }
}
