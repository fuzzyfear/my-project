﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment_5.Custom_Exception;

namespace Assignment_5
{
    public abstract class Requirements
    {
        /// <summary>
        /// Check if the current weapon type is compatible.
        /// </summary>
        /// <param name="weapon"></param>
        /// <param name="character"></param>
        public void CheckWeaponType(Weapon weapon, Character character)
        {
            if (!character.WeaponCompatibility.Contains(weapon.weaponTypes))
                throw new InvalidWeaponException("You are not able to use this type of weapon");
        }

        /// <summary>
        /// Check if the character is high enough level to use the equipment
        /// </summary>
        /// <param name="weapon"></param>
        /// <param name="character"></param>
        public void CheckWeaponLevel(Weapon weapon, Character character)
        {
            if (character.Level < weapon.Level)
                throw new InvalidWeaponException("Your level is too low to use this weapon");
        }

        /// <summary>
        /// Check if the weapon slot is available
        /// </summary>
        /// <param name="weapon"></param>
        /// <param name="character"></param>
        public void CheckWeaponSlot(Weapon weapon, Character character)
        {
            if (character.characterSlots.ContainsKey(weapon.slot))
                throw new InvalidWeaponException("You already have a weapon in this slot");
        }

        /// <summary>
        /// Check if the current weapon type is compatible.
        /// </summary>
        /// <param name="armour"></param>
        /// <param name="character"></param>
        public void CheckArmourType(Armour armour, Character character)
        {
            if (!character.ArmourCompatibility.Contains(armour.armourTypes))
                throw new InvalidArmourException("You are not able to use this type of armour");
        }

        /// <summary>
        /// Check if the character is high enough level to use the equipment
        /// </summary>
        /// <param name="armour"></param>
        /// <param name="character"></param>
        public void CheckArmourLevel(Armour armour, Character character)
        {
            if (character.Level < armour.Level)
                throw new InvalidArmourException("Your level is too low to use this armour");
        }

        /// <summary>
        /// Check if the weapon slot is available
        /// </summary>
        /// <param name="armour"></param>
        /// <param name="character"></param>
        public void CheckArmourSlot(Armour armour, Character character)
        {
            if (character.characterSlots.ContainsKey(armour.slot))
                throw new InvalidArmourException("You already have something equiped in this slot");
        }
    }
}
