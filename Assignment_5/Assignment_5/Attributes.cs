﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_5
{
    public struct PrimaryAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Vitality { get; set; }
        public PrimaryAttributes(int strength, int dexterity, int intelligence, int vitality)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
            Vitality = vitality;
        }

        public static PrimaryAttributes operator +(PrimaryAttributes lhs, PrimaryAttributes rhs)
        {
            return new PrimaryAttributes
            {
                Strength = lhs.Strength + rhs.Strength,
                Dexterity = lhs.Dexterity + rhs.Dexterity,
                Intelligence = lhs.Intelligence + rhs.Intelligence,
                Vitality = lhs.Vitality + rhs.Vitality
            };
        }
    }

    public struct SecondaryAttributes
    {
        public int Health { get; set; }
        public int ArmourRating { get; set; }
        public int ElementalResistance { get; set; }

        public SecondaryAttributes(int health, int armourRating, int elementalResistance)
        {
            Health = health;
            ArmourRating = armourRating;
            ElementalResistance = elementalResistance;
        }
    }
}
