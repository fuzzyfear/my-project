﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_5
{
    public class Weapon: Equipment
    {
        public int Dmg { get; set; }
        public int AttackSpeed { get; set; }
        public WeaponTypes @weaponTypes { get; set; }

        public enum WeaponTypes
        {
            Axes,
            Bows,
            Daggers,
            Hammers,
            Staffs,
            Swords,
            Wands
        }
    }
}
