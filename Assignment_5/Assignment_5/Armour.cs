﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_5
{
    public class Armour : Equipment
    {
        public ArmourTypes @armourTypes { get; set; }

        public enum ArmourTypes
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }
    }
}
