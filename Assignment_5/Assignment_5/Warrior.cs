﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_5
{
    public class Warrior : Character
    {
        public override List<Armour.ArmourTypes> ArmourCompatibility { get; set; } = new()
        {
            Armour.ArmourTypes.Mail,
            Armour.ArmourTypes.Plate
        };
        public override List<Weapon.WeaponTypes> WeaponCompatibility { get; set; } = new()
        {
            Weapon.WeaponTypes.Axes,
            Weapon.WeaponTypes.Hammers,
            Weapon.WeaponTypes.Swords
        };

        public override void CalcDps()
        {
            Equipment equiptedWeapon;
            characterSlots.TryGetValue(Equipment.Slot.Weapons, out equiptedWeapon);
            if (equiptedWeapon != null)
            {
                Weapon weapons = (Weapon)equiptedWeapon;
                DPS = MathF.Floor((weapons.Dmg * weapons.AttackSpeed) * (1 + TotalPrimaryAttributes.Strength * 0.01f));
            }
            else
                DPS = MathF.Floor(1 + TotalPrimaryAttributes.Strength * 0.01f);
        }

        public override void LevelUp(int level)
        {
            try
            {
                CheckLevel(level);
                for (int i = 0; i < level; i++)
                    PrimaryAttributes += new PrimaryAttributes(3, 2, 1, 5);
                Level += level;
                SetTotalPrimaryAttributes();
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public override void SetPrimaryAttributes() => PrimaryAttributes = new PrimaryAttributes(5, 2, 1, 10);
    }
}
