﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_5
{
    public abstract class Equipment
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public Slot slot { get; set; }
        public PrimaryAttributes equipmentAttributes { get; set; }

        public enum Slot
        {
            Head,
            Body,
            Legs,
            Weapons
        }
    }
}
